'''
Epreuve pratique : sujet 2

Mesure de la taille d'un arbre binaire.
'''
# Partie fournie au candidat : aucune modification n'est à apporter


def est_vide(arbre: tuple) -> bool:
    '''Prédicat vrai si et seulement si l'arbre est vide'''
    return arbre == ()


def sous_arbre_gauche(arbre: tuple) -> tuple:
    '''renvoie le sous-arbre gauche d'un arbre non vide'''
    if est_vide(arbre):
        print(arbre)
        raise ValueError("L'arbre vide n'a pas de sous-arbre gauche")
    return arbre[1]


def sous_arbre_droit(arbre: tuple) -> tuple:
    '''renvoie le sous-arbre droit d'un arbre non vide'''
    if est_vide(arbre):
        print(arbre)
        raise ValueError("L'arbre vide n'a pas de sous-arbre droit")
    return arbre[2]


def valeur(arbre: tuple) -> tuple:
    '''renvoie la valeur de l'étiquette d'un arbre non vide'''
    if est_vide(arbre):
        print(arbre)
        raise ValueError("L'arbre vide n'a pas de valeur")
    return arbre[0]


VIDE = ()


# réponses du candidat : rédigez votre code ici


def taille(arbre: tuple) -> int:
    '''calcule la taille d'un arbre binaire'''
    if est_vide(arbre):
        """
        Si l'arbre est vide on renvoie 0
        """
        return 0
    elif est_vide(sous_arbre_gauche(arbre)):
        """
        Si l'arbre est vide à gauche on ne s'occupe plus que du sous_arbre à droite en ajoutant 1 à la taille
        """
        return 1 + taille(sous_arbre_droit(arbre))
    elif est_vide(sous_arbre_droit(arbre)):
        """
        Si l'arbre est vide à droite on ne s'occupe plus que du sous_arbre à gauche en ajoutant 1 à la taille
        """
        return 1 + taille(sous_arbre_gauche(arbre))
    else:
        """
        Si l'arbre contient des sous_arbres à droite et à gauche on s'ocuppe des sous_arbres à gauche et à droite en ajoutant 1 à la taille
        """
        return 1 + taille(sous_arbre_gauche(arbre)) + taille(sous_arbre_droit(arbre))


def hauteur(arbre: tuple) -> int:
    '''calcule la taille d'un arbre binaire'''
    if est_vide(arbre):
        """
        Si l'arbre est vide sa hauteur est de -1
        """
        return -1
    elif est_vide(sous_arbre_droit(arbre)):
        """
        Si l'arbre est vide à droite on ne s'occupe plus que du sous_arbre à gauche en augmentant la hauteur de 1
        """
        return 1 + hauteur(sous_arbre_gauche(arbre))
    elif est_vide(sous_arbre_gauche(arbre)):
        """
        Si l'arbre est vide à gauche on ne s'occupe plus que du sous_arbre à droite en augmentant la hauteur de 1
        """
        return 1 + hauteur(sous_arbre_droit(arbre))
    else:
        """
        Si l'arbre contient des sous_arbres à droite et à gauche on prend le max de la hauteur des deux en ajoutant 1 a la hauteur
        """
        return 1 + max(hauteur(sous_arbre_droit(arbre)), hauteur(sous_arbre_gauche(arbre)))



# tests fournis aux candidat. Rien n'est à modifier.


def tests():
    '''réalise des tests sur les fonctions du candidat'''
    assert taille(VIDE) == 0
    assert hauteur(VIDE) == -1

    arbre_1 = (1, VIDE, VIDE)
    assert taille(arbre_1) == 1
    assert hauteur(arbre_1) == 0

    arbre_2 = (2, VIDE, VIDE)
    assert taille(arbre_2) == 1
    assert hauteur(arbre_2) == 0

    arbre_3 = (3, arbre_1, arbre_2)
    assert taille(arbre_3) == 3
    assert hauteur(arbre_3) == 1

    arbre_4 = (4, arbre_3, arbre_1)
    assert taille(arbre_4) == 5
    assert hauteur(arbre_4) == 2

    arbre_5 = (1, (2, (3, (4, (), ()), ()), (5, (6, (7, (), ()), ()), ())), ())
    assert taille(arbre_5) == 7
    assert hauteur(arbre_5) == 4


if __name__ == "__main__":
    tests()
