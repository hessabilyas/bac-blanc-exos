'''
NSI - épreuve pratique - sujet 1 - exercice 2
'''


class ArbreBinaireRecherche:
    '''
    Classe représentant des arbre binaires de recherche.
    N'implémente pas les méthode permettant d'équilibrer l'arbre.
    '''

    def __init__(self, valeur=None, gauche=None, droite=None):
        if valeur is None:
            self.__valeur = None
        else:
            self.__valeur = valeur
            self.__gauche = gauche
            self.__droite = droite

    def est_vide(self):
        '''Prédicat vrai ssi l'arbre est vide'''
        return self.__valeur is None

    def get_valeur(self):
        '''Renvoie la valeur de l'arbre ssi il n'est pas vide'''
        assert(self.est_vide()) == False 
        return self.__valeur

    def sous_arbre_gauche(self: "ArbreBinaireRecherche") -> "ArbreBinaireRecherche":
        """
        Renvoie le sous arbre gauche de l'arbre présent en argument. 
        Si self._gauche d'un arbre nommé 'arbre' est égale à 5, arbre.sous_arbre_gauche(arbre)
        renverra l'arbre qui a pour self._valeur : 5.
        """
        assert(self.est_vide()) == False
        return self.__gauche

    def sous_arbre_droit(self: "ArbreBinaireRecherche") -> "ArbreBinaireRecherche":
        """
        Renvoie le sous arbre gauche de l'arbre présent en argument. 
        Si self._gauche d'un arbre nommé 'arbre' est égale à 5, arbre.sous_arbre_gauche(arbre)
        renverra l'arbre qui a pour self._valeur : 5.
        """
        assert(self.est_vide()) == False
        return self.__droite

    def inserer(self, cle) -> None:
        '''
        Insère une clé dans l'ABR.
        N'implémente pas l'équilibrage de l'arbre
        '''
        if self.est_vide():
            self.__valeur = cle
            self.__gauche = ArbreBinaireRecherche()
            self.__droite = ArbreBinaireRecherche()
        elif cle < self.get_valeur():
            # on insère à gauche
            self.sous_arbre_gauche().inserer(cle)
        elif cle > self.get_valeur():
            # on insère à droite
            self.sous_arbre_droit().inserer(cle)

    def contient(self: "ArbreBinaireRecherche", cle: int) -> bool:
        '''Prédicat vrai ssi l'arbre contient la cle'''
        if self.est_vide():
            """Si l'arbre est vide la clé ne s'y trouve pas donc on renvoie 'False'"""
            return False 
        elif self.__valeur == cle:
            """Si la valeur de l'arbre est égale à la clé on renvoie 'True'"""
            return True 
        elif self.__valeur < cle:
            """Si la valeur est inférieur à la valeur de l'arbre
            on va dans l'arbre de droite car dans un arbre binaire de recherche la 
            valeur de l'arbre de droite est supérieur à la valeur de l'arbre actuel"""
            return self.sous_arbre_droit().contient(cle) 
        else:
            """Si la valeur est supérieur à la valeur de l'arbre
            on va dans l'arbre de gauche car dans un arbre binaire de recherche 
            la valeur de l'arbre de gauche est inférieur à la valeur de l'arbre
            actuel"""
            return self.sous_arbre_gauche().contient(cle)


VIDE = ArbreBinaireRecherche()


def tester():
    assert VIDE.est_vide()
    arbre_1 = ArbreBinaireRecherche(1,
                                    ArbreBinaireRecherche(),
                                    ArbreBinaireRecherche())
    assert not arbre_1.est_vide()
    assert arbre_1.get_valeur() == 1
    assert arbre_1.sous_arbre_gauche().est_vide()
    assert arbre_1.sous_arbre_droit().est_vide()
    arbre_1.inserer(2)
    assert arbre_1.sous_arbre_gauche().est_vide()
    assert arbre_1.sous_arbre_droit().get_valeur() == 2
    assert arbre_1.contient(1)
    assert arbre_1.contient(2)
    assert not arbre_1.contient(3)


if __name__ == "__main__":
    tester()
