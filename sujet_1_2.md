---
title: "NSI - Terminale - Epreuve pratique"
subtitle: "Sujet 1 - Exercice 2"

---

# Les arbres binaires de recherche (ABR)

Le fichier joint contient une classe et une fonction permettant de la tester.

Cette classe `ArbreBinaireRecherche` implémente des arbres binaires de recherche.

On y trouve les méthodes :

* `__init__` qui permet de construire un arbre qui soit :

    * vide si on crée l'arbre sans paramètre : `ArbreBinaireRecherche()`
    * non vide si on passe trois paramètres : `ArbreBinaireRecherche(valeur, gauche, droit)`

       `valeur` est un entier,
       `gauche` et `droit` sont-eux même des arbres binaires de recherche.
* `est_vide` : prédicat vrai si et seulement-si l'arbre est vide,
* `sous_arbre_gauche` et `sous_arbre_droit` : des accesseurs renvoyant
    les sous-arbres,
* `get_valeur` : qui renvoie la valeur de l'étiquette de la racine de l'arbre,
* `inserer` qui ajoute une feuille à l'arbre binaire de recherche en préservant
    l'ordre,
* `contient` : prédicat vrai si et seulement-si l'arbre contient une clé passée
    en paramètre.

La fonction `tester` génère pour l'instant des erreurs. Elle illustre toutes
les méthodes citées.

_Ce code est incomplet et l'objectif est d'ajouter quelques éléments_

# Consignes

1. Documenter les méthode `sous_arbre_gauche` et `sous_arbre_droit`.
    _Attention : le type renvoyé étant défini DANS la classe elle-même, les indications de type doivent être entre guillemets_. 

2. Les méthodes `get_valeur`, `sous_arbre_gauche` et `sous_arbre_droite` sont insatisfaisantes : elles devraient générer une erreur si l'arbre est vide.
    Ajouter une assertion à ces méthodes.
    _On pourra faire des copiers collers..._

3. La méthode `contient` n'est pas programmée.
    Elle doit renvoyer `True` si un noeud de l'arbre a pour valeur la clé passée en paramètre et `False` sinon.
    Compléter le code de la méthode.

    On rappelle l'algorithme de recherche dans un ABR : 

    ```
    fonction contient (arbre, clé)
            si l'arbre est vide,
                renvoyer Faux
            si la valeur de l'étiquette est clé,
                renvoyer Vrai
            si la valeur est inférieure à la clé,
                renvoyer contient (sous_arbre_droit, clé)
            sinon,
                renvoyer contient (sous_arbre_gauche, clé)
    ```
