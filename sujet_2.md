---
title: "NSI - Terminale - Épreuve pratique"
subtitle: "Sujet 2"
---

## Modalités

* Vous avez une heure pour accomplir ces deux exercices.
* Vous rendez les deux fichiers (`exo1_votre_nom.py` et `exo2_votre_nom.py`) en les déposant dans un dossier portant votre nom sur le bureau. Assurez-vous de rendre les bons fichiers.
* Vous pouvez solliciter l'examinateur, l'échange oral avec l'examinateur fait partie de l'évaluation. 


# Exercice 1 - Restitution d'un algorithme

## Mesures d'un arbre binaire : calcul de sa taille et de sa hauteur.

On considère un arbre binaire tel que celui-ci :

```mermaid
graph TD 

1 --> 2 
1 --> 3 
2 --> 4 
2 --> 5 
3 --> 6 
```


* On rappelle que la _taille_ d'un arbre est donnée par son nombre de noeuds.
* La définition de la _hauteur_ varie d'une source à l'autre. On considère ici
    que la hauteur d'un arbre binaire est le plus grand nombre d'arêtes reliant
    une feuille à la racine.

    Par convention, l'arbre vide a pour hauteur -1.

* Ainsi dans l'exemple ci-dessus, la taille est 6 et la hauteur 2.

Le code fourni contient des fonctions permettant de représenter et manipuler
des arbres binaires représentés à l'aide de _tuples_ Python.

* Les fonctions permettant d'accéder à la valeur de l'étiquette, aux sous-arbres
    gauche et droit sont fournies.

* Une prédicat permet de savoir si un arbre est vide.

* L'arbre vide est représenté par le tuple vide : `()`.


Consignes : 

1. **Écrire une fonction `taille` qui reçoit en paramètre un arbre binaire représenté
    par un tuple et qui renvoie un entier :  sa taille**

    **Votre fonction doit passer les tests exécutés dans le script sans lever d'exception.**


2. **Écrire une fonction `hauteur` qui reçoit en paramètre un arbre binaire représenté
    par un tuple et qui renvoie un entier :  sa hauteur - telle que définie plus haut.**

    **Votre fonction doit passer les tests exécutés dans le script sans lever d'exception.**

**Vos deux fonctions n'utilisent que l'interface fournie et n'accèdent pas
directement aux éléments des tuples.**



